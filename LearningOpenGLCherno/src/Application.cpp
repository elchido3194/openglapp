#include <iostream>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
//#include <glfw3.h>
#include <fstream>
#include <string>
#include <sstream>
struct ShaderProgramSource {
	std::string VertexSource;
	std::string FragmentSource;
};

static ShaderProgramSource ParseShader(const std::string& filepath) {
	std::ifstream stream(filepath);
	enum class ShaderType {
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};

	//Shader type by default
	ShaderType type = ShaderType::NONE;
	std::string line;
	std::stringstream ss[2];
	while (getline(stream, line)) {

		if (line.find("#shader") != std::string::npos) {
			if (line.find("vertex") != std::string::npos)
				type = ShaderType::VERTEX;
			else if (line.find("fragment") != std::string::npos)
				type = ShaderType::FRAGMENT;
		}
		else {
			ss[(int)type] << line << '\n';
		}
	}

	return { ss[0].str(), ss[1].str() };

}
unsigned int compileShader(unsigned int type, const std::string& source ) {
	unsigned int shader = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(shader, 1, &src, nullptr);
	glCompileShader(shader);
	int result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		int lenght;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &lenght);
		char* message = (char*)alloca(lenght * sizeof(char));
		glGetShaderInfoLog(shader, lenght, &lenght, message);
		std::cout << "Failed to Compile " << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << "shader" << std::endl;
		std::cout << message << std::endl;
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}

unsigned int CreateShaders(const std::string& vertexshader, const std::string& fragmentshader) {
	unsigned int program = glCreateProgram();
	unsigned int vshader = compileShader(GL_VERTEX_SHADER, vertexshader);
	unsigned int fshader = compileShader(GL_FRAGMENT_SHADER, fragmentshader);
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glLinkProgram(program);
	glValidateProgram(program);

	glDeleteShader(vshader);
	glDeleteShader(fshader);
	return program;

}


int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(1200, 700, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();	
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	//Inicializa GLEW luego de la generacion del contexto de OPENGL
	if (glewInit() != GLEW_OK)
		std::cout << "Error while initializing GLEW" << std::endl;

	std::cout << glGetString(GL_VERSION) << std::endl;

	float positions[6]{
		-0.5f, -0.5f,
		 0.5f, -0.5f,
		 0.0f,  0.5f
	};

	//First we should create and bind the vertex buffer
	unsigned int buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	//With the buffer binded, we can tell OpenGL how the buffer should read data and it's layout
	//First the data
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), positions, GL_STATIC_DRAW);
	//Then the layout
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2*sizeof(float), 0);
	//We should enable the attributes
	glEnableVertexAttribArray(0);

	
	ShaderProgramSource source = ParseShader("res/shaders/Basic.shader");
	std::cout << "Vertex" << std::endl;
	std::cout << source.VertexSource<< std::endl;
	std::cout << "Fragment" << std::endl;
	std::cout << source.FragmentSource << std::endl;

	//unsigned int shader = CreateShaders(vertexShader, fragmentShader);
	//glUseProgram(shader);

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}